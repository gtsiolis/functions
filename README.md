## A simple `echo` function for node.js to get started with GitLab Serverless.

Visit the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) for more information.